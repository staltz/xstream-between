# `xstream-between`

An xstream operator to filter for events between two stream events.

```
npm install --save xstream-between
```

## description

Filter for events from `source` that occur between the latest event from `first` and the latest event from `second`.

```
source: --a--b----c----d---e-f--g----h---i--j-----
first:  -------F------------------F---------------
second: -----------------S-----------------S------
                        between
output: ----------c----d-------------h---i--------
```

## usage

```js
import between from 'xstream-between';

const output$ = source$.compose(between(first$, second$));
```

## License

MIT

