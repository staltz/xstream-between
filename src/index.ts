import {Stream} from 'xstream';

/**
 * source: --a--b----c----d---e-f--g----h---i--j-----
 * first:  -------F------------------F---------------
 * second: -----------------S-----------------S------
 *                         between
 * output: ----------c----d-------------h---i--------
 */
export default function between<T>(first: Stream<any>, second: Stream<any>) {
  return (source: Stream<T>) => first.mapTo(source.endWhen(second)).flatten();
}

